Lee un archivo CSV y publica su contenido en una cola JMS de HornetQ.

    mvn clean package
    java -jar target\hq-producer.jar
    
El archivo generado es fat-jar.

Links de interés:

- https://nofluffjuststuff.com/blog/bruce_snyder/2011/08/tuning_jms_message_consumption_in_spring
- https://dzone.com/articles/hornetq-getting-started
- http://hornetq.jboss.org/downloads.html
- http://www.baeldung.com/apache-commons-csv
- https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/jms/listener/DefaultMessageListenerContainer.html
 
