package com.certicamara.certifactura.infraestructura.hornetq.batch.mensajes;

import java.util.List;

import co.s4n.osp.dto.DataTransferObject;

/**
 * CertiFactura Certicámara S.A. ContarDocumentosReporteDTO
 * 
 * @author Seven4N Ltda. Jan 10, 2013
 */
public class MensajeBatchAdquirienteDTO implements DataTransferObject
{

	// ------------------------------
	// Atributos
	// ------------------------------

	private static final long serialVersionUID = -1093357654039205672L;

	private String id;

	private String idCliente;
	
	private String identificadorCertifactura;

	private String IdSolicitud;

	private String acuerdoFisicoFacturacionElectronica;

	private String adjuntarPdfNotificaciones;

	private String adjuntarXmlNotificaciones;

	private String apellidos;

	private String cantidadDiasAceptacionAutomatica;
	
	private String codigoPais;

	private String codigoCiudad;

	private String codigoDepartamento;
	
	private String ciudadExtranjera;

	private String direccion;

	private String emailPrincipal;

	private List< String > emailSecundarios;

	private String enviarCorreoDeBienvenida;

	private String enviarNotificaciones;

	private String fax;

	private String codigoDian;

	private String digitoDeVerificacion;

	private String numeroIdentificacion;

	private String naturaleza;

	private String nombre;

	private String observaciones;

	private String razonSocial;

	private String telefono;

	private String contrasena;

	private String generarContrasena;

	private String nombreUsuario;

	private String estado;
	
	private String registradoCatalogo;
	
	private String procesarXML;
	
	private String entregaMail;
	
	private String entregaFisica;
	
	private String entregaPlataforma;
	
	private String correoCertificado;
	
	private String codigoSucursal;
	
	private String nombreSucursal;
	
	private String codigoCentroCosto;
	
	private String nombreCentroCosto;

	private List<String> listaRepresentacionPersona;
	
	private List<String> listaObligacionesAdquiriente;
	
	private List<String> listaUsuariosAduaneros;
	
	private List<String> listaEstablecimientos;
		

	List< CamposAdicionalesAdquiriente > camposAdicionalesAdquirientes;

	/**
	 * Constructor
	 */
	public MensajeBatchAdquirienteDTO( )
	{
		super( );
	}

	/**
	 * Constructor
	 */
	public MensajeBatchAdquirienteDTO( String idCliente, String idSolicitud, String acuerdoFisicoFacturacionElectronica, String adjuntarPdfNotificaciones
			                         , String adjuntarXmlNotificaciones, String apellidos, String cantidadDiasAceptacionAutomatica, String codigoPais, String codigoCiudad
			                         , String codigoDepartamento, String ciudadExtranjera, String direccion, String emailPrincipal, List< String > emailSecundarios
			                         , String enviarCorreoDeBienvenida, String enviarNotificaciones, String fax, String codigoDian, String digitoDeVerificacion
			                         , String numeroIdentificacion, String naturaleza, String nombre, String observaciones, String razonSocial, String telefono
			                         , String contrasena, String generarContrasena, String nombreUsuario, String registradoCatalogo, String procesarXML
			                         , String entregaMail, String entregaFisica, String entregaPlataforma, String correoCertificado, List< CamposAdicionalesAdquiriente > camposAdicionalesAdquirientes )
	{
		super( );
		this.idCliente = idCliente;
		IdSolicitud = idSolicitud;
		this.acuerdoFisicoFacturacionElectronica = acuerdoFisicoFacturacionElectronica;
		this.adjuntarPdfNotificaciones = adjuntarPdfNotificaciones;
		this.adjuntarXmlNotificaciones = adjuntarXmlNotificaciones;
		this.apellidos = apellidos;
		this.cantidadDiasAceptacionAutomatica = cantidadDiasAceptacionAutomatica;
		this.codigoPais = codigoPais;
		this.codigoCiudad = codigoCiudad;
		this.codigoDepartamento = codigoDepartamento;
		this.ciudadExtranjera = ciudadExtranjera;
		this.direccion = direccion;
		this.emailPrincipal = emailPrincipal;
		this.emailSecundarios = emailSecundarios;
		this.enviarCorreoDeBienvenida = enviarCorreoDeBienvenida;
		this.enviarNotificaciones = enviarNotificaciones;
		this.fax = fax;
		this.codigoDian = codigoDian;
		this.digitoDeVerificacion = digitoDeVerificacion;
		this.numeroIdentificacion = numeroIdentificacion;
		this.naturaleza = naturaleza;
		this.nombre = nombre;
		this.observaciones = observaciones;
		this.razonSocial = razonSocial;
		this.telefono = telefono;
		this.contrasena = contrasena;
		this.generarContrasena = generarContrasena;
		this.nombreUsuario = nombreUsuario;
		this.registradoCatalogo = registradoCatalogo;
		this.procesarXML = procesarXML;
		this.entregaMail = entregaMail;
		this.entregaFisica = entregaFisica;
		this.entregaPlataforma = entregaPlataforma;
		this.camposAdicionalesAdquirientes = camposAdicionalesAdquirientes;
		this.correoCertificado = correoCertificado;
	}

	/**
	 * @return the id
	 */
	public String getId( )
	{
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId( String id )
	{
		this.id = id;
	}

	/**
	 * @return the idCliente
	 */
	public String getIdCliente( )
	{
		return idCliente;
	}

	/**
	 * @param idCliente
	 *            the idCliente to set
	 */
	public void setIdCliente( String idCliente )
	{
		this.idCliente = idCliente;
	}
		
	/**
	 * @return the identificadorCliente
	 */
	public String getIdentificadorCertifactura( )
	{
		return identificadorCertifactura;
	}

	/**
	 * @param identificadorCliente the identificadorCliente to set
	 */
	public void setIdentificadorCertifactura( String identificadorCliente )
	{
		this.identificadorCertifactura = identificadorCliente;
	}

	/**
	 * @return the idSolicitud
	 */
	public String getIdSolicitud( )
	{
		return IdSolicitud;
	}

	/**
	 * @param idSolicitud
	 *            the idSolicitud to set
	 */
	public void setIdSolicitud( String idSolicitud )
	{
		IdSolicitud = idSolicitud;
	}

	/**
	 * @return the acuerdoFisicoFacturacionElectronica
	 */
	public String isAcuerdoFisicoFacturacionElectronica( )
	{
		return acuerdoFisicoFacturacionElectronica;
	}

	/**
	 * @param acuerdoFisicoFacturacionElectronica
	 *            the acuerdoFisicoFacturacionElectronica to set
	 */
	public void setAcuerdoFisicoFacturacionElectronica( String acuerdoFisicoFacturacionElectronica )
	{
		this.acuerdoFisicoFacturacionElectronica = acuerdoFisicoFacturacionElectronica;
	}

	/**
	 * @return the adjuntarPdfNotificaciones
	 */
	public String isAdjuntarPdfNotificaciones( )
	{
		return adjuntarPdfNotificaciones;
	}

	/**
	 * @param adjuntarPdfNotificaciones
	 *            the adjuntarPdfNotificaciones to set
	 */
	public void setAdjuntarPdfNotificaciones( String adjuntarPdfNotificaciones )
	{
		this.adjuntarPdfNotificaciones = adjuntarPdfNotificaciones;
	}

	/**
	 * @return the adjuntarXmlNotificaciones
	 */
	public String isAdjuntarXmlNotificaciones( )
	{
		return adjuntarXmlNotificaciones;
	}

	/**
	 * @param adjuntarXmlNotificaciones
	 *            the adjuntarXmlNotificaciones to set
	 */
	public void setAdjuntarXmlNotificaciones( String adjuntarXmlNotificaciones )
	{
		this.adjuntarXmlNotificaciones = adjuntarXmlNotificaciones;
	}

	/**
	 * @return the apellidos
	 */
	public String getApellidos( )
	{
		return apellidos;
	}

	/**
	 * @param apellidos
	 *            the apellidos to set
	 */
	public void setApellidos( String apellidos )
	{
		this.apellidos = apellidos;
	}

	/**
	 * @return the cantidadDiasAceptacionAutomatica
	 */
	public String getCantidadDiasAceptacionAutomatica( )
	{
		return cantidadDiasAceptacionAutomatica;
	}

	/**
	 * @param cantidadDiasAceptacionAutomatica
	 *            the cantidadDiasAceptacionAutomatica to set
	 */
	public void setCantidadDiasAceptacionAutomatica( String cantidadDiasAceptacionAutomatica )
	{
		this.cantidadDiasAceptacionAutomatica = cantidadDiasAceptacionAutomatica;
	}
	
	/**
	 * @return the codigoPais
	 */
	public String getCodigoPais() {
		return codigoPais;
	}

	/**
	 * @param codigoPais the codigoPais to set
	 */
	public void setCodigoPais(String codigoPais) {
		this.codigoPais = codigoPais;
	}

	/**
	 * @return the codigoCiudad
	 */
	public String getCodigoCiudad( )
	{
		return codigoCiudad;
	}

	/**
	 * @param codigoCiudad
	 *            the codigoCiudad to set
	 */
	public void setCodigoCiudad( String codigoCiudad )
	{
		this.codigoCiudad = codigoCiudad;
	}

	/**
	 * @return the codigoDepartamento
	 */
	public String getCodigoDepartamento( )
	{
		return codigoDepartamento;
	}

	/**
	 * @param codigoDepartamento
	 *            the codigoDepartamento to set
	 */
	public void setCodigoDepartamento( String codigoDepartamento )
	{
		this.codigoDepartamento = codigoDepartamento;
	}
	
	/**
	 * @return the ciudadExtranjera
	 */
	public String getCiudadExtranjera() {
		return ciudadExtranjera;
	}

	/**
	 * @param ciudadExtranjera the ciudadExtranjera to set
	 */
	public void setCiudadExtranjera(String ciudadExtranjera) {
		this.ciudadExtranjera = ciudadExtranjera;
	}

	/**
	 * @return the direccion
	 */
	public String getDireccion( )
	{
		return direccion;
	}

	/**
	 * @param direccion
	 *            the direccion to set
	 */
	public void setDireccion( String direccion )
	{
		this.direccion = direccion;
	}

	/**
	 * @return the emailPrincipal
	 */
	public String getEmailPrincipal( )
	{
		return emailPrincipal;
	}

	/**
	 * @param emailPrincipal
	 *            the emailPrincipal to set
	 */
	public void setEmailPrincipal( String emailPrincipal )
	{
		this.emailPrincipal = emailPrincipal;
	}

	/**
	 * @return the emailSecundarios
	 */
	public List< String > getEmailSecundarios( )
	{
		return emailSecundarios;
	}

	/**
	 * @param emailSecundarios
	 *            the emailSecundarios to set
	 */
	public void setEmailSecundarios( List< String > emailSecundarios )
	{
		this.emailSecundarios = emailSecundarios;
	}

	/**
	 * @return the enviarCorreoDeBienvenida
	 */
	public String isEnviarCorreoDeBienvenida( )
	{
		return enviarCorreoDeBienvenida;
	}

	/**
	 * @param enviarCorreoDeBienvenida
	 *            the enviarCorreoDeBienvenida to set
	 */
	public void setEnviarCorreoDeBienvenida( String enviarCorreoDeBienvenida )
	{
		this.enviarCorreoDeBienvenida = enviarCorreoDeBienvenida;
	}

	/**
	 * @return the enviarNotificaciones
	 */
	public String isEnviarNotificaciones( )
	{
		return enviarNotificaciones;
	}

	/**
	 * @param enviarNotificaciones
	 *            the enviarNotificaciones to set
	 */
	public void setEnviarNotificaciones( String enviarNotificaciones )
	{
		this.enviarNotificaciones = enviarNotificaciones;
	}

	/**
	 * @return the fax
	 */
	public String getFax( )
	{
		return fax;
	}

	/**
	 * @param fax
	 *            the fax to set
	 */
	public void setFax( String fax )
	{
		this.fax = fax;
	}

	/**
	 * @return the codigoDian
	 */
	public String getCodigoDian( )
	{
		return codigoDian;
	}

	/**
	 * @param codigoDian
	 *            the codigoDian to set
	 */
	public void setCodigoDian( String codigoDian )
	{
		this.codigoDian = codigoDian;
	}

	/**
	 * @return the digitoDeVerificacion
	 */
	public String getDigitoDeVerificacion( )
	{
		return digitoDeVerificacion;
	}

	/**
	 * @param digitoDeVerificacion
	 *            the digitoDeVerificacion to set
	 */
	public void setDigitoDeVerificacion( String digitoDeVerificacion )
	{
		this.digitoDeVerificacion = digitoDeVerificacion;
	}

	/**
	 * @return the numeroIdentificacion
	 */
	public String getNumeroIdentificacion( )
	{
		return numeroIdentificacion;
	}

	/**
	 * @param numeroIdentificacion
	 *            the numeroIdentificacion to set
	 */
	public void setNumeroIdentificacion( String numeroIdentificacion )
	{
		this.numeroIdentificacion = numeroIdentificacion;
	}

	/**
	 * @return the naturaleza
	 */
	public String getNaturaleza( )
	{
		return naturaleza;
	}

	/**
	 * @param naturaleza
	 *            the naturaleza to set
	 */
	public void setNaturaleza( String naturaleza )
	{
		this.naturaleza = naturaleza;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre( )
	{
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre( String nombre )
	{
		this.nombre = nombre;
	}

	/**
	 * @return the observaciones
	 */
	public String getObservaciones( )
	{
		return observaciones;
	}

	/**
	 * @param observaciones
	 *            the observaciones to set
	 */
	public void setObservaciones( String observaciones )
	{
		this.observaciones = observaciones;
	}

	/**
	 * @return the razonSocial
	 */
	public String getRazonSocial( )
	{
		return razonSocial;
	}

	/**
	 * @param razonSocial
	 *            the razonSocial to set
	 */
	public void setRazonSocial( String razonSocial )
	{
		this.razonSocial = razonSocial;
	}

	/**
	 * @return the telefono
	 */
	public String getTelefono( )
	{
		return telefono;
	}

	/**
	 * @param telefono
	 *            the telefono to set
	 */
	public void setTelefono( String telefono )
	{
		this.telefono = telefono;
	}

	/**
	 * @return the contrasena
	 */
	public String getContrasena( )
	{
		return contrasena;
	}

	/**
	 * @param contrasena
	 *            the contrasena to set
	 */
	public void setContrasena( String contrasena )
	{
		this.contrasena = contrasena;
	}

	/**
	 * @return the generarContrasena
	 */
	public String isGenerarContrasena( )
	{
		return generarContrasena;
	}

	/**
	 * @param generarContrasena
	 *            the generarContrasena to set
	 */
	public void setGenerarContrasena( String generarContrasena )
	{
		this.generarContrasena = generarContrasena;
	}

	/**
	 * @return the nombreUsuario
	 */
	public String getNombreUsuario( )
	{
		return nombreUsuario;
	}

	/**
	 * @param nombreUsuario
	 *            the nombreUsuario to set
	 */
	public void setNombreUsuario( String nombreUsuario )
	{
		this.nombreUsuario = nombreUsuario;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid( )
	{
		return serialVersionUID;
	}

	/**
	 * @return the camposAdicionalesAdquirientes
	 */
	public List< CamposAdicionalesAdquiriente > getCamposAdicionalesAdquirientes( )
	{
		return camposAdicionalesAdquirientes;
	}

	/**
	 * @param camposAdicionalesAdquirientes
	 *            the camposAdicionalesAdquirientes to set
	 */
	public void setCamposAdicionalesAdquirientes( List< CamposAdicionalesAdquiriente > camposAdicionalesAdquirientes )
	{
		this.camposAdicionalesAdquirientes = camposAdicionalesAdquirientes;
	}

	/**
	 * @return the estado
	 */
	public String getEstado( )
	{
		return estado;
	}

	/**
	 * @param estado
	 *            the estado to set
	 */
	public void setEstado( String estado )
	{
		this.estado = estado;
	}
	
	/**
	 * 
	 * @return
	 */
	public String isEntregaMail() {
		return entregaMail;
	}

	/**
	 * 
	 * @param entregaMail
	 */
	public void setEntregaMail(String entregaMail) {
		this.entregaMail = entregaMail;
	}

	/**
	 * 
	 * @return
	 */
	public String isEntregaFisica() {
		return entregaFisica;
	}

	/**
	 * 
	 * @param entregaFisica
	 */
	public void setEntregaFisica(String entregaFisica) {
		this.entregaFisica = entregaFisica;
	}

	/**
	 * 
	 * @return
	 */
	public String isEntregaPlataforma() {
		return entregaPlataforma;
	}

	/**
	 * 
	 * @param entregaPlataforma
	 */
	public void setEntregaPlataforma(String entregaPlataforma) {
		this.entregaPlataforma = entregaPlataforma;
	}

	/**
	 * 
	 * @return
	 */
	public String isRegistradoCatalogo() {
		return registradoCatalogo;
	}

	/**
	 * 
	 * @param registradoCatalogo
	 */
	public void setRegistradoCatalogo(String registradoCatalogo) {
		this.registradoCatalogo = registradoCatalogo;
	}

	/**
	 * 
	 * @return
	 */
	public String isProcesarXML() {
		return procesarXML;
	}

	/**
	 * 
	 * @param procesarXML
	 */
	public void setProcesarXML(String procesarXML) {
		this.procesarXML = procesarXML;
	}
	
	public String getCorreoCertificado() {
		return correoCertificado;
	}

	public void setCorreoCertificado(String correoCertificado) {
		this.correoCertificado = correoCertificado;
	}

	public String getAcuerdoFisicoFacturacionElectronica() {
		return acuerdoFisicoFacturacionElectronica;
	}

	public String getAdjuntarPdfNotificaciones() {
		return adjuntarPdfNotificaciones;
	}

	public String getAdjuntarXmlNotificaciones() {
		return adjuntarXmlNotificaciones;
	}

	public String getEnviarCorreoDeBienvenida() {
		return enviarCorreoDeBienvenida;
	}

	public String getEnviarNotificaciones() {
		return enviarNotificaciones;
	}

	public String getGenerarContrasena() {
		return generarContrasena;
	}

	public String getRegistradoCatalogo() {
		return registradoCatalogo;
	}

	public String getProcesarXML() {
		return procesarXML;
	}

	public String getEntregaMail() {
		return entregaMail;
	}

	public String getEntregaFisica() {
		return entregaFisica;
	}

	public String getEntregaPlataforma() {
		return entregaPlataforma;
	}

	public List<String> getListaRepresentacionPersona() {
		return listaRepresentacionPersona;
	}

	public void setListaRepresentacionPersona(
			List<String> listaRepresentacionPersona) {
		this.listaRepresentacionPersona = listaRepresentacionPersona;
	}

	public List<String> getListaObligacionesAdquiriente() {
		return listaObligacionesAdquiriente;
	}

	public void setListaObligacionesAdquiriente(
			List<String> listaObligacionesAdquiriente) {
		this.listaObligacionesAdquiriente = listaObligacionesAdquiriente;
	}

	public List<String> getListaUsuariosAduaneros() {
		return listaUsuariosAduaneros;
	}

	public void setListaUsuariosAduaneros(List<String> listaUsuariosAduaneros) {
		this.listaUsuariosAduaneros = listaUsuariosAduaneros;
	}

	public List<String> getListaEstablecimientos() {
		return listaEstablecimientos;
	}

	public void setListaEstablecimientos(List<String> listaEstablecimientos) {
		this.listaEstablecimientos = listaEstablecimientos;
	}
	
	public String getCodigoSucursal() {
		return codigoSucursal;
	}

	public void setCodigoSucursal(String codigoSucursal) {
		this.codigoSucursal = codigoSucursal;
	}

	public String getNombreSucursal() {
		return nombreSucursal;
	}

	public void setNombreSucursal(String nombreSucursal) {
		this.nombreSucursal = nombreSucursal;
	}

	public String getCodigoCentroCosto() {
		return codigoCentroCosto;
	}

	public void setCodigoCentroCosto(String codigoCentroCosto) {
		this.codigoCentroCosto = codigoCentroCosto;
	}

	public String getNombreCentroCosto() {
		return nombreCentroCosto;
	}

	public void setNombreCentroCosto(String nombreCentroCosto) {
		this.nombreCentroCosto = nombreCentroCosto;
	}

	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */

	@Override
	public String toString() {
		return "MensajeBatchAdquirienteDTO [id=" + id + ", idCliente=" + idCliente + ", identificadorCertifactura="
				+ identificadorCertifactura + ", IdSolicitud=" + IdSolicitud + ", acuerdoFisicoFacturacionElectronica="
				+ acuerdoFisicoFacturacionElectronica + ", adjuntarPdfNotificaciones=" + adjuntarPdfNotificaciones
				+ ", adjuntarXmlNotificaciones=" + adjuntarXmlNotificaciones + ", apellidos=" + apellidos
				+ ", cantidadDiasAceptacionAutomatica=" + cantidadDiasAceptacionAutomatica + ", codigoPais="
				+ codigoPais + ", codigoCiudad=" + codigoCiudad + ", codigoDepartamento=" + codigoDepartamento
				+ ", ciudadExtranjera=" + ciudadExtranjera + ", direccion=" + direccion + ", emailPrincipal="
				+ emailPrincipal + ", emailSecundarios=" + emailSecundarios + ", enviarCorreoDeBienvenida="
				+ enviarCorreoDeBienvenida + ", enviarNotificaciones=" + enviarNotificaciones + ", fax=" + fax
				+ ", codigoDian=" + codigoDian + ", digitoDeVerificacion=" + digitoDeVerificacion
				+ ", numeroIdentificacion=" + numeroIdentificacion + ", naturaleza=" + naturaleza + ", nombre=" + nombre
				+ ", observaciones=" + observaciones + ", razonSocial=" + razonSocial + ", telefono=" + telefono
				+ ", contrasena=" + contrasena + ", generarContrasena=" + generarContrasena + ", nombreUsuario="
				+ nombreUsuario + ", estado=" + estado + ", registradoCatalogo=" + registradoCatalogo + ", procesarXML="
				+ procesarXML + ", entregaMail=" + entregaMail + ", entregaFisica=" + entregaFisica
				+ ", entregaPlataforma=" + entregaPlataforma + ", correoCertificado=" + correoCertificado
				+ ", codigoSucursal=" + codigoSucursal + ", nombreSucursal=" + nombreSucursal + ", codigoCentroCosto="
				+ codigoCentroCosto + ", nombreCentroCosto=" + nombreCentroCosto + ", listaRepresentacionPersona="
				+ listaRepresentacionPersona + ", listaObligacionesAdquiriente=" + listaObligacionesAdquiriente
				+ ", listaUsuariosAduaneros=" + listaUsuariosAduaneros + ", listaEstablecimientos="
				+ listaEstablecimientos + ", camposAdicionalesAdquirientes=" + camposAdicionalesAdquirientes + "]";
	}
}
