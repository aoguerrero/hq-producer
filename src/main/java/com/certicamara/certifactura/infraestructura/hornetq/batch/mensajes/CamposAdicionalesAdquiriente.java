package com.certicamara.certifactura.infraestructura.hornetq.batch.mensajes;

import co.s4n.osp.dto.DataTransferObject;

/**
 * 
 * CertiFactura Certicámara S.A. CampoAdicionalesCliente
 * 
 * @author Seven4N Ltda. May 17, 2012
 */
public class CamposAdicionalesAdquiriente implements DataTransferObject
{

	// ------------------------------
	// Atributos
	// ------------------------------
	/**
	 * 
	 */

	private String identificador;
	private Object tipo;
	private String valor;

	/**
	 * Constructor
	 */
	public CamposAdicionalesAdquiriente( )
	{
		super( );
	}

	/**
	 * Constructor
	 */
	public CamposAdicionalesAdquiriente( String identificador, Object tipo, String valor )
	{
		super( );
		this.identificador = identificador;
		this.tipo = tipo;
		this.valor = valor;
	}

	/**
	 * @return the identificador
	 */
	public String getIdentificador( )
	{
		return identificador;
	}

	/**
	 * @param identificador
	 *            the identificador to set
	 */
	public void setIdentificador( String identificador )
	{
		this.identificador = identificador;
	}

	/**
	 * @return the tipo
	 */
	public Object getTipo( )
	{
		return tipo;
	}

	/**
	 * @param tipo
	 *            the tipo to set
	 */
	public void setTipo( Object tipo )
	{
		this.tipo = tipo;
	}

	/**
	 * @return the valor
	 */
	public String getValor( )
	{
		return valor;
	}

	/**
	 * @param valor
	 *            the valor to set
	 */
	public void setValor( String valor )
	{
		this.valor = valor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString( )
	{
		return "CamposAdicionalesAdquiriente [identificador=" + identificador + ", tipo=" + tipo + ", valor=" + valor + ", getIdentificador()=" + getIdentificador( ) + ", getTipo()=" + getTipo( ) + ", getValor()=" + getValor( ) + ", getClass()=" + getClass( ) + ", hashCode()=" + hashCode( ) + ", toString()=" + super.toString( ) + "]";
	}

}
