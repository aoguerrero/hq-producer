package com.certicamara.certifactura.infraestructura.hornetq.mensajes.facturaElectronica;

import co.s4n.osp.dto.DataTransferObject;

/**
 * Mensaje utilizado para solicitar la generación del PDF de la factura electrónica
 * CertiFactura
 * Certicámara S.A.
 * MensajeGenerarPDFFacturaElectronica
 * @author Seven4N Ltda.
 * 2012-08-08
 */
public class MensajeGenerarPDFFacturaElectronica implements DataTransferObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7410691974439865800L;
	
	
	private String idEmisor;
	private String idReceptor;
	private String idCliente;
	private String idFactura;

	/**
	 * Constructor
	 */
	public MensajeGenerarPDFFacturaElectronica( )
	{
	}
	
	/**
	 * Constructor
	 */
	public MensajeGenerarPDFFacturaElectronica( String idEmisor, String idReceptor, String idCliente, String idFactura )
	{
		super( );
		this.idEmisor = idEmisor;
		this.idReceptor = idReceptor;
		this.idCliente = idCliente;
		this.idFactura = idFactura;
	}
	
	

	/**
	 * @return the idEmisor
	 */
	public String getIdEmisor( )
	{
		return idEmisor;
	}

	/**
	 * @param idEmisor the idEmisor to set
	 */
	public void setIdEmisor( String idEmisor )
	{
		this.idEmisor = idEmisor;
	}

	/**
	 * @return the idReceptor
	 */
	public String getIdReceptor( )
	{
		return idReceptor;
	}

	/**
	 * @param idReceptor the idReceptor to set
	 */
	public void setIdReceptor( String idReceptor )
	{
		this.idReceptor = idReceptor;
	}

	/**
	 * @return the idCliente
	 */
	public String getIdCliente( )
	{
		return idCliente;
	}

	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente( String idCliente )
	{
		this.idCliente = idCliente;
	}

	/**
	 * @return the idFactura
	 */
	public String getIdFactura( )
	{
		return idFactura;
	}

	/**
	 * @param idFactura the idFactura to set
	 */
	public void setIdFactura( String idFactura )
	{
		this.idFactura = idFactura;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString( )
	{
		return "MensajeGenerarPDFFacturaElectronica [idEmisor=" + idEmisor + ", idReceptor=" + idReceptor + ", idCliente=" + idCliente + ", idFactura=" + idFactura + "]";
	}
}