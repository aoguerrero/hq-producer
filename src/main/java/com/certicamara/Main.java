package com.certicamara;

import java.io.FileReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import com.certicamara.certifactura.infraestructura.hornetq.batch.mensajes.MensajeBatchAdquirienteDTO;

public class Main {

	public static void main(String[] args) throws Exception {

		String idCliente = args[0];
		String rutaCsv = args[1];

		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.PROVIDER_URL, "jnp://localhost:1099");
		env.put(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
		env.put(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");
		Context ctx = new InitialContext(env);

		ConnectionFactory cf = (ConnectionFactory) ctx.lookup("/ConnectionFactory");

		Queue queue = (Queue) ctx.lookup("/queue/actualizarBatchAdquirienteQueue");

		Connection connection = cf.createConnection();
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

		MessageProducer producer = session.createProducer(queue);

		Reader in = new FileReader(rutaCsv);
		Iterable<CSVRecord> records = CSVFormat.newFormat(';').withFirstRecordAsHeader().withTrim().parse(in);
		
		/*

		0	naturaleza
		1	codigoDIAN
		2	numeroIdentificacion
		3	digitoVerificacion
		4	nombre
		5	apellido
		6	razonSocial
		7	codigoSucursal
		8	nombreSucursal
		9	codigoPais
		10	codigoDepartamento
		11	codigoCiudad
		12	nombreCiudadExtranjera
		13	direccion
		14	telefono
		15	codigoCentroCosto
		16	nombreCentroCosto
		17	emailPrincipal
		18	emailSecundario
		19	usuario
		20	clave
		21	generarClave
		22	observaciones
		23	adjuntarPDF
		24	adjuntarXML
		25	registradoCatalogoParticipantes
		26	entregaMail
		27	entregaFisica
		28	correoCertificado
		29	tipoRepresentacion
		30	tipoObligacion
		31	tipoUsuarioAduanero
		32	tipoEstablecimiento
		33	entregaPlataforma

		*/
		for (CSVRecord record : records) {
			
			String emailSecundario = record.get(18);
			List<String> emailSecundarioList;
			if(emailSecundario.trim().length() > 0 && emailSecundario != null) {
				emailSecundarioList = Arrays.asList(emailSecundario.split(","));
			} else {
				emailSecundarioList = Arrays.asList(new String[]{});
			}

			ObjectMessage message = session.createObjectMessage();
			
			String idSolicitud = "";
			
			MensajeBatchAdquirienteDTO obj = new MensajeBatchAdquirienteDTO(
					idCliente, 
					idSolicitud, 
					"false", 
					record.get(23),
					record.get(24), 
					record.get(5), 
					"3", 
					record.get(9), 
					record.get(11),
					record.get(10), 
					record.get(12), 
					record.get(13),
					record.get(17), 
					emailSecundarioList,
					"true", 
					"true", 
					"", 
					record.get(1), 
					record.get(3),
					record.get(2), 
					record.get(0), 
					record.get(4), 
					record.get(22), 
					record.get(6), 
					record.get(14),
					record.get(20), 
					record.get(21), 
					record.get(19), 
					record.get(25), 
					"false",
					record.get(26), 
					record.get(27), 
					record.get(33), 
					record.get(28), 
					null 
			);
			message.setObject(obj);
			producer.send(message);
			System.out.println("> Sent to queue: '" + obj + "'");
		}
		connection.close();
	}
}