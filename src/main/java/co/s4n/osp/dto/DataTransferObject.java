package co.s4n.osp.dto;

import java.io.Serializable;

public abstract interface DataTransferObject
  extends Serializable
{}

/* Location:           D:\certifactura\CertiFactura_Lib\lib\otros\OSP.jar
 * Qualified Name:     co.s4n.osp.dto.DataTransferObject
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */